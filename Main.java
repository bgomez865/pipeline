/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
//TFG 2013-2014

import files.ConfigFile;
import architecture.Architecture;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Belen Bermejo Gonzalez
 */

/*Class that implements the main program*/
public class Main {

    static String path;       
    static String configPath = "config.properties";
    static String outFile;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        ConfigFile conf = new ConfigFile(configPath);
        conf.loadConfig();
        // conf.showConfig();
        String option = ConfigFile.getRandomFile();
        switch (option) {
            case "y":               
                break;
            case "n":
                path = ConfigFile.getSourcePath();
                //  System.out.println("PATH: " + path);
                break;
            default:
                System.out.println("OPTION ERROR");
        }
        Architecture a = new Architecture(ConfigFile.getnBits(), path);
        Pipeline p = new Pipeline();
        a.simulateMIPS(p);
    }
}

//TFG 2013-2014
package main;

import architecture.Architecture;
import architecture.Instruction;
import architecture.MIPS;
import java.io.IOException;
import java.util.ArrayList;
import files.ConfigFile;


/**
 *
 * @author Belen Bermejo Gonzalez
 */

/*Class to implements the pipelined execution*/
public class Pipeline implements MIPS {

    public enum pipelineStages {

        FETCH, DECODE, EXECUTION, MEMORY, WRITE, COMMIT
    }

    private static ArrayList<Instruction> listOfIntructions = new ArrayList<>();
    private final int SCALAB = ConfigFile.getScalability();

    //Stage latency
    private final int FETCH = ConfigFile.getFetchCycles();
    private final int DECODE = ConfigFile.getDecodeCycles();
    private final int EXECUTE = ConfigFile.getExecuteCycles();
    private final int MEMORY = ConfigFile.getMemoryCycles();
    private final int WRITE = ConfigFile.getWriteCycles();
    private final int COMMIT = ConfigFile.getCommitCycles();

    public void initPipe() {
        listOfIntructions = Architecture.getListOfIntructions();               
    }

    public void executePipe() throws IOException {
        initPipe();
        
    }

    public void fetch() {
        
    }

    public void decode() {
        
    }

    public void execute() {
        
    }

    public void memory() {
        
    }

    public void write() {
       
    }
}

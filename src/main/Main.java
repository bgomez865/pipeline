package main;
//TFG 2013-2014

import files.ConfigFile;
import architecture.Architecture;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Práctica Arquitectura de computadores. La practica consiste en hacer la
 * la simulación de una máquina segmentada. Teniendo en cuenta las dependencias,
 * los branches y la escalabilidad como punto mas importantes a parte de la
 * ejecución en si de manera normal. No se tendra en cuenta el forwarding
 * y las dependencias que se tengan en cuenta seran binarias.
 * 
 * @author Belen Bermejo Gonzalez
 * @author Enrique Piña
 * @author Lluc Martorell
 */
public class Main {

    static String path;       
    static String configPath = "config.properties";
    static String outFile;

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {
        ConfigFile conf = new ConfigFile(configPath);
        conf.loadConfig();
        // conf.showConfig();
        String option = ConfigFile.getRandomFile();
        switch (option) {
            case "y":               
                break;
            case "n":
                path = ConfigFile.getSourcePath();
                //  System.out.println("PATH: " + path);
                break;
            default:
                System.out.println("OPTION ERROR");
        }
        
        Architecture a = new Architecture(ConfigFile.getnBits(), path);
        Pipeline p = new Pipeline();
        a.simulateMIPS(p);
    }   
}

package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JPanel;

/**
 * Clase encargada de ibujar la simulacion de la ejecucion de determinado
 * programa de la maquina MIPS. Se le pasa la información necesaria 
 * de la clase de Pipeline y unicamente se dibuja.
 * 
 * @author Enrique Piña 
 * @author Lluc Martorell
 */
public class TimeLine extends JPanel {

    /*Constantes*/
    private final int X_MARGIN = 200;       // Margen horizontal del dibujo de ciclos y fases
    private final int Y_MARGIN = 60;        // Margen vertical del dibujo de los ciclos
    private final int INST_X_MARGIN = 20;   // Margen horizontal para dibujar la instrucción
    private final int RECT_WIDTH = 80;      // Tamaño horizontal de los rectangulos de las fases
    private final int RECT_HEIGHT = 25;     // Tamaño vertical de los rectangulos de las fases
    private final int CEL_HEIGHT = 60;      // Separación vertical entre barras divisorias de instrucciones
    
    /*Variables*/
    private HashMap<Integer, ReducedInstruction> map = new HashMap<>(); // Instrucciones reducidas ejecutadas por orden.
    private final ArrayList<Integer> fuOfInst;                          // Unidad funcional con la que es ejecutada cada instrucción.
    private final float realizedBranches;                               // Porcentade de branchs realiazos.
    
    /**
     * Metodo constructor. Inicializamos todos los parametros necesarios para
     * dibujar la grafica y se pone el tamaño necesario del panel dependiendo
     * del numero de instrucciones ejecutada y el numero de ciclos totales
     *
     * @param map                               Hash con la instruccion reducida, en orden de ejecución.
     * @param functionalUnityofInst             Unidad funcional que se usa por instruccion ejecutada en el mismo orden de ejecucion
     * @param percentageOfRealizedBranches      Percentaje de branchs realizados
     */
    public TimeLine(HashMap<Integer, ReducedInstruction> map, ArrayList<Integer> functionalUnityofInst, float percentageOfRealizedBranches) {
        this.map = map;
        this.fuOfInst = functionalUnityofInst;
        this.realizedBranches = percentageOfRealizedBranches;
        setPreferredSize(new Dimension(X_MARGIN+((getLastCycle()+1)*RECT_WIDTH),Y_MARGIN+((this.map.size()+1)*CEL_HEIGHT)));
    }

    /**
     * Metodo para pintar todo el panel. Se empieza pintando el total de ciclos,
     * las lineas divisorias horizontales, despues el nombre de las instrucciones por 
     * orden de ejecución y información de la unidad funcional que la ejecuta, se
     * sigue pintando los rectangulos de las fases y lo stalls y se acaba con
     * la información extra: CPIs y porcentaje de branchs realizados.
     * 
     * @param g     Componente gráfica para pintar el panel
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        
        /*Total de ciclos*/
        int cyclesMargin = X_MARGIN;
        for(int i=0; i<=getLastCycle(); i++){
            g2.drawString(""+i, cyclesMargin, Y_MARGIN);
            cyclesMargin += RECT_WIDTH;
        }
        
        /*Lineas horizontales*/
        int heightLines = Y_MARGIN + 15;
        for(int i=0; i<=map.size(); i++){
            Line2D lin = new Line2D.Float(X_MARGIN, heightLines, cyclesMargin-70, heightLines);
            g2.draw(lin);
            heightLines += CEL_HEIGHT;
        }
        
        /*Nombre y Uf de las instrucciones*/
        int heightInstructions = Y_MARGIN+INST_X_MARGIN+(CEL_HEIGHT/2);
        int nInst = 0;
        for(ReducedInstruction i : map.values()){
            g2.drawString("UF: "+fuOfInst.get(nInst)+" | "+i.toString(), INST_X_MARGIN, heightInstructions);
            heightInstructions += CEL_HEIGHT;
            nInst++;
        }
        
        /*Stalls y fases*/
        int heightBlock = 92;
        int start = 0, finish = 0, extraMargin = 0;
        String fase = "";
        for (ReducedInstruction ins : map.values()) {
            
            /*Por cada instrucción se dibujaran sus 5 fases*/
            for (int j = 0 ; j < 5 ; j++) {
                switch (j) {
                    case 0:                 
                        g.setColor(Color.YELLOW);
                        fase = "IF"; extraMargin = 35;
                        start = ins.getSfetch();
                        finish = ins.getFetch();
                        break;
                    case 1:
                        fase = "ID"; extraMargin = 35;
                        start = ins.getSdecode();
                        /*Si el principio de la fase de decode y el final de la fase de fetch
                        son distintas se dibujaran tantos Stalls como ciclos vacios haya*/
                        if (start != finish) {
                            g.setColor(Color.BLACK);
                            for(int i = finish; i < start; i++) {
                                g.drawString("STALL", i * RECT_WIDTH + X_MARGIN + 25, heightBlock+18);
                            }
                        }
                        finish = ins.getDecode();
                        g.setColor(Color.RED);
                        break;
                    case 2:
                        g.setColor(Color.CYAN);
                        fase = "EX"; extraMargin = 32;
                        start = ins.getSexec();
                        finish = ins.getExec();
                        break;
                    case 3:
                        g.setColor(Color.MAGENTA);
                        fase = "MEM"; extraMargin = 25;
                        start = ins.getSmem();
                        finish = ins.getMem();                                                
                        break;
                    case 4:
                        g.setColor(Color.GREEN);
                        fase = "WB"; extraMargin = 30;
                        start = ins.getSwr();
                        finish = ins.getWr();                                                
                        break;
                }             
                
                /*Cada fase puede tener un numero incierto de ciclos, se dibujaran
                tantos rectangulos como ciclos haya para la fase determinada*/
                for (int i = start; i < finish; i++) {
                    Rectangle r = new Rectangle(i * RECT_WIDTH + X_MARGIN, heightBlock, RECT_WIDTH, RECT_HEIGHT);
                    g.drawRect(r.x, r.y, r.width, r.height);
                    g.fillRect(r.x, r.y, r.width, r.height);
                    Color aux = g.getColor();
                    g.setColor(Color.BLACK);
                    g.drawString(fase, i * RECT_WIDTH + X_MARGIN + extraMargin, heightBlock+18);
                    g.setColor(aux);
                }
            }
            heightBlock += CEL_HEIGHT;              
        }
        
        /*CPIs*/
        Rectangle r = new Rectangle(110, 5, 60, 25);
        g.setColor(Color.BLACK);
        g2.drawString("CPIs", 5, 22);
        g.drawRect(r.x, r.y, r.width, r.height); 
        g.fillRect(r.x, r.y, r.width, r.height);
        g.setColor(Color.WHITE);
        g2.drawString(String.format("%.2f", (float)getLastCycle()/(float)map.size()), 120, 22);
        
        /*Porcentaje saltos realizados*/
        Rectangle r2 = new Rectangle(110, 40, 60, 25);
        g.setColor(Color.BLACK);
        g2.drawString("Taken branches", 5, 57);        
        g.drawRect(r2.x, r2.y, r2.width, r2.height); 
        g.fillRect(r2.x, r2.y, r2.width, r2.height);
        g.setColor(Color.WHITE);
        g2.drawString(String.format("%.2f", realizedBranches*100)+"%", 114, 57);
        
    }
    
    private int getLastCycle(){       
        int aux = 0;
        for (ReducedInstruction i : map.values()) {
            if (aux < i.getWr()) aux = i.getWr();
        }
        return aux;
    }
}